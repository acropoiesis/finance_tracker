class User::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit({ roles: [] }, :first_name, :last_name, :email, :password, :current_password) }
    devise_parameter_sanitizer.permit(:account_update) { |u| u.permit({ roles: [] }, :first_name, :last_name, :email, :password, :current_password) }
  end

end